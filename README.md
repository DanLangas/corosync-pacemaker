# Corosync-pacemaker role
## Install
### Install python (ubuntu)
For use ansible, you need install python2.7 on remote server.
```bash
ansible xen -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-apt;"
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash
allow_world_readable_tmpfiles=True

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```

### download xen role
```bash
mkdir ./roles
cat <<'EOF' >> ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/corosync-pacemaker.git
  name: corosync-pacemaker
EOF
echo "roles/corosync-pacemaker" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```
## Hosts example
```
[corosync-pacemaker]
gw01 ansible_host=192.168.1.2 master=true
gw02 ansible_host=192.168.1.3
```

## Playbook example
Install and setup Xen server.
```bash
cat << 'EOF' > corosync-pacemaker.yml
---
- hosts:
    corosync-pacemaker
  become: yes
  become_user: root
  tasks:
    - name: corosync-pacemaker
      include_role:
        name: corosync-pacemaker
      vars:
        action: install
        hacluster_passwd: "123"
        cluster_nodes: '192.168.1.2 192.168.1.3'
        cluster_name: "super-duper-cluster"

    - name: corosync-pacemaker cluster settings
      include_role:
        name: corosync-pacemaker
      vars:
        action: cluster_cmd
        command: "{{ item }}"
      with_items:
        - "property set default-resource-stickiness=INFINITY"
        - "property set no-quorum-policy=ignore"
        - "property set stonith-enabled=false"
      when:
        - master is defined

    - name: corosync-pacemaker resource_IPaddr2
      include_role:
        name: corosync-pacemaker
      vars:
        action: cluster_cmd
        command: "resource create r-vip-eth0 ocf:heartbeat:IPaddr2 ip=192.168.1.1 nic=eth0 cidr_netmask=24 op monitor interval=1s --group megagroup"
      when:
        - master is defined
EOF

ansible-playbook ./corosync-pacemaker.yml
```
